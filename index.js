function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    // Checks letter length if it is a single character
    if(letter.length === 1){
        // for every element in a sentence
        for(i in sentence){
            if(letter === sentence[i]){
                // increment if the element matches the letter
                result += 1
            }
        }
        // returns count
        return result
    } else {
        // if invalid, return undefined
        return undefined
    }
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    result = 0;

    for(i in text){
        // if the first index matches the last index, there is no repeating letter
    if(text.indexOf(text[i]) === text.lastIndexOf(text[i])){
            result = true
    } else {
            result = false
            // breaks the loop if matching letters are found
            break;
        }
    }

    return result
    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    if(age<13){
        return undefined;
    } else if ((age>=13 && age<=21) || age>=65) {
        // if 13 to 21, or senior citizen, apply 20% discount which is 100%-20% = 80%, thus price*80
        return String(Math.round((price*.80)*100)/100);
    } else {
        return String(Math.round(price*100)/100);
    }
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    hotCategories = []

    function checkStocks(item){
        if(item.stocks === 0){
            return item
        }
    }
    
    hotItems = items.filter(checkStocks) //filter items that have no more stocks
    
    // for all items that have 0 stocks
    for(x in hotItems){
        // if category not yet in hotCategories array, push the category
        if (hotCategories.indexOf(hotItems[x].category) === -1){ 
            hotCategories.push(hotItems[x].category)
        }
    }

    return hotCategories; 

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    flyingVoters = []

    for(x in candidateA){
        if(candidateB.indexOf(candidateA[x]) !== -1){
            flyingVoters.push(candidateA[x])
        }
    }

    return flyingVoters;


}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};